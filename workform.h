#ifndef WORKFORM_H
#define WORKFORM_H

#include <QWidget>
#include <QTableView>
#include "dialog.h"
#include "editdoctableform.h"
#include "editpacientstableform.h"
#include "editappointmentsform.h"
#include "best_encryptoclass.h"

namespace Ui {
class WorkForm;
}
class Dialog;

class WorkForm : public QWidget
{
    Q_OBJECT

public:

    void *SetDialogForm(Dialog *dio)
    { this->D = dio; }

    explicit WorkForm(QWidget *parent = 0);
    ~WorkForm();

    WorkForm *GetThisWorkForm()
    {return this;}

public slots:
    void RefreshData();
    void AddDataToFirstTable();
    void AddDataToPacientsTable();
    void AddDataToPositionsTable();
    void AddDataToAppointmentsTable();
    void AddDataToAppointmentsViewTable();
    void AddDataToDocTable();
    void AddDataToDocViewTable();

private slots:

    void on_AddUserButton_clicked();

    void on_DeleteButton_clicked();

    void on_EditUserButton_clicked();

    void on_DBData_Table_tabBarClicked(int index);

    void on_EditDocTable_clicked();

    void on_Edit_Pacients_Button_clicked();

    void on_EditPos_clicked();

    void on_AddPos_clicked();

    void on_DeletePos_clicked();

    void on_EditAppointmentsTableButtonEditAppointmentsTableButton_clicked();

    void on_SearchDocTableButton_clicked();

    void on_SearchPositionsButton_clicked();

    void on_SearchPacientsTableButton_clicked();

    void on_SearchAppointmentsTableButton_clicked();

    void on_SearchAppointmentsViewTableButton_clicked();

    void on_SearchDocViewTableButton_clicked();

    void on_SearchAccountsTableButton_clicked();

    void on_AutisticsDevsCopy_linkActivated(const QString &link);

private:
    Ui::WorkForm *ui;
    Dialog *D;
    QSqlQueryModel DocAccountsData;
    QSqlQueryModel DocData;
    QSqlQueryModel PacientsData;
    QSqlQueryModel PositionsData;
    QSqlQueryModel AppointmentsData;
    QSqlQueryModel AppointmentsViewData;
    QSqlQueryModel DocViewData;
    Best_EncryptoClass *Encrypted;
    QSqlDatabase db;

};

#endif // WORKFORM_H
