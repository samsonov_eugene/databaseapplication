#ifndef EDITDOCTABLEFORM_H
#define EDITDOCTABLEFORM_H

#include <QDialog>
#include "dialog.h"
#include "workform.h"

namespace Ui {
class EditDocTableForm;
}
class WorkForm;
class Dialog;

class EditDocTableForm : public QDialog
{
    Q_OBJECT

public:

    void *SetWorkForm(WorkForm *dio)
    { this->WF = dio; }

    explicit EditDocTableForm(QWidget *parent = 0);

    bool CheckSpaceClick();
    ~EditDocTableForm();

private slots:
    void on_AddButton_clicked();

    void on_EditButton_clicked();

    void on_DelButton_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_AddTime_clicked();

    void on_EditTime_clicked();

    void on_ReturnToTable_clicked();

    void on_AddCab_clicked();

    void on_EditCab_clicked();

    void on_Doc_Number_editingFinished();

private:
    Ui::EditDocTableForm *ui;
    WorkForm *WF;
    QSqlDatabase db;
};

#endif // EDITDOCTABLEFORM_H
