#include "best_encryptoclass.h"
#include <QDebug>

QString Best_EncryptoClass::encryptoString(const QString& str)
{
    bestReadXML *BRX = new bestReadXML();
    BRX->parseKeyXML();
    QByteArray t = str.toUtf8();
    QMessageAuthenticationCode code(QCryptographicHash::Sha256);
    code.setKey(BRX->getKey());
    code.addData(t);
    delete(BRX);
    return code.result().toHex();
}
