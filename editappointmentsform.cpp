#include "editappointmentsform.h"
#include "ui_editappointmentsform.h"

QString EditAppointmentsForm::CurrentDocNumber = "#";

void EditAppointmentsForm::getDocNumber()
{
    if (ui->DocNumber_box->currentText()!="Выберите врача")
    {
        auto TMPSplitData = ui->DocNumber_box->currentText().split("№");
        this->CurrentDocNumber = TMPSplitData.at(1);
    }
    else
    {
       this->CurrentDocNumber = "#";
       ui->Appointment_Time->clear();
    }
}

void EditAppointmentsForm::addDataToDocnumberbox()
{
    ui->DocNumber_box->clear();
    QSqlQuery qry(db);
    qry.prepare("SELECT doctors.First_Name, doctors.Last_Name, doctors.Doctor_Number FROM doctors INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                "WHERE positions.Position = :DocType");
    qry.bindValue(":DocType", ui->DocType_box->currentText());
    qry.exec();
    ui->DocNumber_box->addItem("Выберите врача");
    while (qry.next())
        ui->DocNumber_box->addItem(qry.value(0).toString() + " " + qry.value(1).toString() + " №" + qry.value(2).toString());
    qDebug() << qry.lastError() << " - " << ui->DocType_box->currentText();
}

void EditAppointmentsForm::addDataToDoctypebox()
{
    ui->DocType_box->clear();
    QSqlQuery qry(db);
    qry.prepare("SELECT Position FROM positions WHERE Position <> 'Регистратор'");
    qry.exec();
    ui->DocType_box->addItem("Выберите специализацию");
    while (qry.next())
        ui->DocType_box->addItem(qry.value(0).toString());
}

EditAppointmentsForm::EditAppointmentsForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditAppointmentsForm)
{
    ui->setupUi(this);
    Dialog *D;
    db = D->Getdb();
    ui->Appointment_Date->setEnabled(false);
    QDate currentDate;
    //ui->Appointment_Date->setDate(currentDate.currentDate());
    ui->Appointment_Num->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->Birth_Certificate_Num->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,15}"), this));
    ui->NumDelAppointment->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
}

EditAppointmentsForm::~EditAppointmentsForm()
{
    delete ui;
}

void EditAppointmentsForm::on_EditAppointmentButton_clicked()
{
    QSqlQuery qry(db);
    QSqlQuery qry_mini(db);
    QSqlQuery qry_minis(db);
    qry_minis.prepare("SELECT Cabinet_Number FROM cabinets WHERE Doctor_Number = :DN");
    qry_minis.bindValue(":DN", this->CurrentDocNumber.toInt());
    qry_minis.exec();
    if ((ui->DocNumber_box->currentText().isEmpty()) && (ui->Birth_Certificate_Num->text().isEmpty()) && (ui->Appointment_Date->text().isEmpty()) && (ui->Appointment_Num->text().isEmpty()))
    {
        QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
    }
    else
    {
        if ((ui->DocNumber_box->currentText()!="Выберите врача") && (ui->DocNumber_box->currentText() != ""))
        {
            if (ui->Appointment_Time->count() != 0)
            {
                if (qry_minis.first())
                {
                    qry_mini.prepare("SELECT * FROM vacations WHERE Doctor_Number = :DN");
                    qry_mini.bindValue(":DN", this->CurrentDocNumber.toInt());
                    qry_mini.exec();
                    qry_mini.first();
                    QDate CURD = CURD.currentDate();
                    QTime CurTime = CurTime.currentTime();
                    QTime ChoosedTime = QTime::fromString(ui->Appointment_Time->currentText(),"hh:mm:ss");
                    if ((ui->Appointment_Date->date() <= CURD) && (ChoosedTime < CurTime))
                    {
                        QMessageBox::information(this,"НЕ ОК", "Добавление записей в прошлое недопустимо.");
                    }
                    else
                    {
                        if ((qry_mini.value(1).toDate() <= ui->Appointment_Date->date()) && (ui->Appointment_Date->date() <= qry_mini.value(2).toDate()))
                            QMessageBox::information(this,"НЕ ОК", "В выбранное вами время данного врача нет на месте");
                        else
                        {
                            qry.prepare("UPDATE appointments SET Policy_Number = :BCN, Doctor_Number = :DN, Date_Of_Receipt = :AD, Time_Of_Receipt = :TOR WHERE Appointment_Number = :AN");
                            qry.bindValue(":BCN", ui->Birth_Certificate_Num->text());
                            qry.bindValue(":DN", this->CurrentDocNumber.toInt());
                            qry.bindValue(":AD", ui->Appointment_Date->text());
                            qry.bindValue(":TOR", ui->Appointment_Time->currentText());
                            qry.bindValue(":AN", ui->Appointment_Num->text());
                            if (qry.exec())
                            {
                                this->WF->RefreshData();
                                QMessageBox::information(this,"ОК", "Редактирование успешно");
                            }
                            else
                            {
                                qDebug() << qry.lastError().text();
                                QStringList ERROR = qry.lastError().text().split("'");
                                if (ERROR.count()!= 5)
                                {
                                    QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Возможно, что такого пациента не существует");
                                }
                                else
                                {
                                  if (ERROR.at(3) == "appointments_Policy_Number_IDX")
                                    QMessageBox::information(this,"НЕ ОК", "У этого пациента уже есть запись на это время");
                                  else if (ERROR.at(3) == "appointments_Date_Of_Receipt_IDX")
                                    QMessageBox::information(this,"НЕ ОК", "У этого врача уже есть запись на это время");
                                  else if (ERROR.at(3) == "PRIMARY")
                                    QMessageBox::information(this,"НЕ ОК", "Запись с таким номером существует");
                                  else
                                    QMessageBox::information(this,"НЕ ОК", "Произошла неизвестная ошибка");
                                }
                            }
                        }
                    }
                } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что у этого врача отсутствует кабинет"); }
            } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что у этого врача отсутствует время работы"); }
        } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что вы не выбрали врача"); }
    }
}

void EditAppointmentsForm::on_AddAppointment_Button_clicked()
{
    QSqlQuery qry(db);
    QSqlQuery qry_mini(db);
    QSqlQuery qry_minis(db);
    qry_minis.prepare("SELECT Cabinet_Number FROM cabinets WHERE Doctor_Number = :DN");
    qry_minis.bindValue(":DN", this->CurrentDocNumber.toInt());
    qry_minis.exec();
    if ((ui->DocNumber_box->currentText().isEmpty()) && (ui->Birth_Certificate_Num->text().isEmpty()) && (ui->Appointment_Date->text().isEmpty()) && (ui->Appointment_Num->text().isEmpty()))
    {  QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");    }
    else
    {
        if ((ui->DocNumber_box->currentText()!="Выберите врача") && (ui->DocNumber_box->currentText() != ""))
        {
            if (ui->Appointment_Time->count() != 0)
            {
                if (qry_minis.first())
                {
                    qry_mini.prepare("SELECT * FROM vacations WHERE Doctor_Number = :DN");
                    qry_mini.bindValue(":DN", this->CurrentDocNumber.toInt());
                    qry_mini.exec();
                    qry_mini.first();
                    QDate CURD = CURD.currentDate();
                    QTime CurTime = CurTime.currentTime();
                    QTime ChoosedTime = QTime::fromString(ui->Appointment_Time->currentText(),"hh:mm:ss");
                    if ((ui->Appointment_Date->date() <= CURD) && (ChoosedTime < CurTime))
                    {
                        QMessageBox::information(this,"НЕ ОК", "Добавление записей в прошлое недопустимо.");
                    }
                    else
                    {
                        if ((qry_mini.value(1).toDate() <= ui->Appointment_Date->date()) && (ui->Appointment_Date->date() <= qry_mini.value(2).toDate()))
                            QMessageBox::information(this,"НЕ ОК", "В выбранное вами время данного врача нет на месте");
                        else
                        {
                            qry.prepare("INSERT INTO appointments (Appointment_Number, Policy_Number, Doctor_Number,"
                                        " Date_Of_Receipt, Time_Of_Receipt) VALUES(:AppNum, :BCN, :DN, :DOR, :TOR)");
                            qry.bindValue(":AppNum", ui->Appointment_Num->text());
                            qry.bindValue(":BCN", ui->Birth_Certificate_Num->text());
                            qry.bindValue(":DN", this->CurrentDocNumber.toInt());
                            qry.bindValue(":DOR", ui->Appointment_Date->text());
                            qry.bindValue(":TOR", ui->Appointment_Time->currentText());
                            if (qry.exec())
                            {
                                this->WF->RefreshData();
                                QMessageBox::information(this,"ОК", "Запись добавлена");
                            }
                            else
                            {
                                QStringList ERROR = qry.lastError().text().split("'");
                                if (ERROR.count()!= 5)
                                {
                                    QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Возможно, что такого пациента не существует");
                                }
                                else
                                  if (ERROR.at(3) == "appointments_Policy_Number_IDX")
                                    QMessageBox::information(this,"НЕ ОК", "У этого пациента уже есть запись на это время");
                                  else if (ERROR.at(3) == "appointments_Date_Of_Receipt_IDX")
                                    QMessageBox::information(this,"НЕ ОК", "У этого врача уже есть запись на это время");
                                  else if (ERROR.at(3) == "PRIMARY")
                                    QMessageBox::information(this,"НЕ ОК", "Запись с таким номером существует");
                                  else
                                    QMessageBox::information(this,"НЕ ОК", "Произошла неизвестная ошибка");
                            }
                        }
                    }
                } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что у этого врача отсутствует кабинет"); }
            } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что у этого врача нет времени работы"); }
        } else { QMessageBox::information(this,"НЕ ОК", "Произошла ошибка, возможно, что вы не выбрали врача"); }
    }
}

void EditAppointmentsForm::on_DeleteAppointmentButton_clicked()
{
    if (!ui->NumDelAppointment->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("DELETE FROM appointments WHERE Appointment_Number = :NDA");
        qry.bindValue(":NDA", ui->NumDelAppointment->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Записи с таким номером не существует");
            else
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Запись удалена");
            }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Во время удаления произошла ошибка");
            qDebug() << qry.lastError();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили поле, необходимое для удаления.");
}

void EditAppointmentsForm::on_exitButton_clicked()
{
    this->WF->RefreshData();
    this->hide();
}

void EditAppointmentsForm::on_DocType_box_activated(const QString &arg1)
{
    ui->Appointment_Time->clear();
    ui->DocNumber_box->clear();
    this->addDataToDocnumberbox();
}

void EditAppointmentsForm::on_DocNumber_box_activated(const QString &arg1)
{
    QDate currentDate;
    ui->Appointment_Time->clear();
    ui->Appointment_Date->clear();
    ui->Appointment_Date->setDate(currentDate.currentDate());
    ui->Appointment_Date->setEnabled(true);
    this->addDataToDocTimebox();
}

struct ExistsTimeEnum
{
    QString DocNum;
    QDate Date_Of_Receipt;
    QTime Time_Of_Receipt;
};

void EditAppointmentsForm::addDataToDocTimebox()
{
    this->getDocNumber();
    /***/
    QVector <ExistsTimeEnum> ExistsTime;
    ExistsTimeEnum ETE;
    QSqlQuery qry_mini(db);
    qry_mini.prepare("SELECT Doctor_Number, Date_Of_Receipt, Time_Of_Receipt FROM appointments WHERE (Doctor_Number = :DocNum)");
    qry_mini.bindValue(":DocNum", this->CurrentDocNumber.toInt());
    qry_mini.exec();
    while (qry_mini.next())
    {
        ETE.DocNum = qry_mini.value(0).toString();
        ETE.Date_Of_Receipt = qry_mini.value(1).toDate();
        ETE.Time_Of_Receipt = qry_mini.value(2).toTime();
        ExistsTime.push_back(ETE);
    }
    /***/
    ui->Appointment_Time->clear();
    QSqlQuery qry(db);
    qry.prepare("SELECT Start_Time, End_Time FROM working_hours WHERE Doctor_Number = :DocNum");
    qry.bindValue(":DocNum", this->CurrentDocNumber.toInt());
    qry.exec();
    qry.first();
    QVector <QTime> WorkingTimeShow;
    for (auto i = qry.value(0).toTime(); i < qry.value(1).toTime(); i = i.addSecs(30 * 60))
    {
        WorkingTimeShow.push_back(i);
    }

    for (int i = 0; i < WorkingTimeShow.count(); i++)
    {
        bool copy = false;
        for (int j = 0; j < ExistsTime.count(); j++)
        {
            if ((ExistsTime.at(j).Time_Of_Receipt == WorkingTimeShow.at(i)) && (ExistsTime.at(j).Date_Of_Receipt == ui->Appointment_Date->date()))
                copy = true;
        }
        if (!copy)
        {
            ui->Appointment_Time->addItem(WorkingTimeShow.at(i).toString());
        }
    }
}

void EditAppointmentsForm::on_Birth_Certificate_Num_editingFinished()
{
    QDate currentDate;
    ui->Appointment_Time->clear();
    ui->DocType_box->clear();
    ui->DocNumber_box->clear();
    this->addDataToDoctypebox();
    ui->Appointment_Date->clear();
    ui->Appointment_Date->setDate(currentDate.currentDate());
    ui->Appointment_Date->setEnabled(false);
}

void EditAppointmentsForm::on_Appointment_Date_editingFinished()
{
    ui->Appointment_Time->repaint();
    addDataToDocTimebox();
}

void EditAppointmentsForm::on_printButton_clicked()
{
    if (!ui->NumDelAppointment->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("SELECT pacients.Policy_Number, pacients.Last_Name, pacients.First_Name, pacients.Patronymic, doctors.Last_Name, doctors.First_Name, doctors.Patronymic, "
                      "positions.Position, cabinets.Cabinet_Number, appointments.Time_Of_Receipt, appointments.Date_Of_Receipt "
                      "FROM appointments "
                      "INNER JOIN doctors ON appointments.Doctor_Number = doctors.Doctor_Number "
                      "INNER JOIN pacients ON appointments.Policy_Number = pacients.Policy_Number "
                      "INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                      "INNER JOIN cabinets ON cabinets.Doctor_Number = doctors.Doctor_Number "
                      "WHERE Appointment_Number = :Num");
        qry.bindValue(":Num", ui->NumDelAppointment->text());
        if (qry.exec())
        {
            if (qry.first())
            {
                QPrinter printer;
                printer.setPrinterName("BestOfTheBestPrinter");
                QPrintDialog dialog(&printer, this);

                if (dialog.exec() == QDialog::Accepted)
                {
                    QTextDocument *TD = new QTextDocument();
                    TD->setHtml(showHTMLPrintSource(ui->NumDelAppointment->text(), qry.value(0).toString(), qry.value(1).toString() + " " + qry.value(2).toString() + " " + qry.value(3).toString(),
                                                    qry.value(4).toString() + " " + qry.value(5).toString() + " " + qry.value(6).toString(), qry.value(7).toString(),
                                                    qry.value(10).toString(), qry.value(9).toString(), qry.value(8).toString()));
                    TD->print(&printer);
                }
            } else { QMessageBox::information(this,"НЕ ОК", "Такой записи не существует"); }
        } else { QMessageBox::information(this,"НЕ ОК", "Произошла неизвестная ошибка"); }
    } else { QMessageBox::information(this,"НЕ ОК", "Вы не заполнили номер записи. Заполните его пожалуйста"); }
}

QString EditAppointmentsForm::showHTMLPrintSource(const QString &Number, const QString &PolicyNumber, const QString &pacient, const QString &Doc, const QString &Specialisation, const QString &Data, const QString &time,
                         const QString &CabNum)
{
    QString Source =
    "<table style= 'border-collapse: collapse; width: 100%; height: 100%; ' border= '1'>"
        "<tbody>"
            "<tr style= 'height: 238px; '>"
                "<td style= 'width: 586px; height: 238px; '>"
                    "<p style= 'text-align: left; '>&nbsp;</p>"
                    "<h2 style= 'text-align: center; '><span style= 'text-decoration: underline; '>&nbsp;<strong>Талон на прием ко врачу № "+Number+"</strong>&nbsp;</span></h2>"
                    "<p style= 'text-align: left; '>&nbsp;</p>"
                    "<table style= 'border-collapse: collapse; width: 100%; ' border= '0'>"
                        "<tbody>"
                            "<tr style= 'height: 20px; '>"
                                "<td style= 'height: 20px; text-align: right; width: 30%; vertical-align: middle; '><strong>Номер полиса - </strong></td>"
                                "<td style= 'width: 40.969%; height: 20px; text-align: left; '>"+PolicyNumber+"</td>"
                                "<th style= 'width: 25.2426%; text-align: center; vertical-align: middle; height: 136px; ' rowspan= '7 '><img src= 'qrc:/new/prefix1/test.png' alt= ' ' width= '198 ' height= '148 ' /></th>"
                            "</tr>"
                            "<tr style= 'height: 20px; '>"
                                "<td style= 'height: 20px; text-align: right; width: 30%; vertical-align: middle; '><strong>Пациент</strong> - </td>"
                                "<td style= 'width: 40.969%; height: 20px; text-align: left; '>"+pacient+"</td>"
                            "</tr>"
                            "<tr style= 'height: 20px; '>"
                                "<td style= 'height: 20px; text-align: right; width: 30%; vertical-align: middle; '><strong>Врач</strong> - </td>"
                                "<td style= 'width: 40.969%; height: 20px; text-align: left; '>"+Doc+"</td>"
                            "</tr>"
                            "<tr style= 'height: 20px; '>"
                                "<td style= 'height: 20px; text-align: right; width: 30%; vertical-align: middle; '><strong>Специализация</strong> - </td>"
                                "<td style= 'width: 40.969%; height: 20px; text-align: left; '>"+Specialisation+"</td>"
                            "</tr>"
                            "<tr>"
                                "<td style = 'text-align: right;'><strong>Дата</strong> - </td>"
                                "<td style = 'text-align: left;'>"+Data+"</td>"
                            "</tr>"
                            "<tr>"
                                "<td style = 'text-align: right;'><strong>Время</strong> - </td>"
                                "<td style = 'text-align: left;'>"+time+"</td>"
                            "</tr>"
                           "<tr>"
                               "<td style = 'text-align: right;'><strong>Кабинет</strong> - </td>"
                               "<td style = 'text-align: left;'>"+CabNum+"</td>"
                           "</tr>"
                        "</tbody>"
                    "</table>"
                    "<p style= 'text-align: center; '>Пожалуйста, не опаздывайте!</p>"
                    "<p style= 'text-align: center; '>Для отмены записи или переноса на другое время обратитесь в регистратуру.</p>"
                    "<p style= 'text-align: left; '>&nbsp;</p>"
                "</td>"
            "</tr>"
            "<tr>"
                "<td style= 'width: 586px; text-align: center; '>(c) Autistics Developers Inc. 2018</td>"
            "</tr>"
        "</tbody>"
    "</table>"
    "<p>&nbsp;</p>";
    return Source;
}
