#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMessageBox>
#include <QTextCodec>
#include <QDebug>
#include "workform.h"
#include "ui_dialog.h"
#include "bestreadxml.h"
#include "best_encryptoclass.h"
namespace Ui {
class Dialog;
}
class WorkForm;

class Dialog : public QDialog
{
    Q_OBJECT

public:

    WorkForm *SetWorkForm(WorkForm *dio)
    { this->WF = dio; }

    WorkForm *GetWorkForm()
    { return this->WF;}

    explicit Dialog(QWidget *parent = 0);

    ~Dialog();

    QString GetUsername()
    {return username;}

    QSqlDatabase Getdb()
    {return db;}

    QString GetAccess()
    {return Access;}

    QString DoctorNumber()
    {return _DoctorNumber;}

    Best_EncryptoClass *getEncrypted()
    {return Encrypted;}

private slots:
    void on_AuthorizationButton_clicked();

private:
    Ui::Dialog *ui;
    WorkForm *WF;
    static QSqlDatabase db;
    static bool CheckConnectionToDB;
    static QString Access;
    static QString username;
    static QString _DoctorNumber;
    static Best_EncryptoClass *Encrypted;
};

#endif // DIALOG_H
