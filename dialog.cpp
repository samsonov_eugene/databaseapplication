﻿#include "dialog.h"
#include "ui_dialog.h"

Best_EncryptoClass *Dialog::Encrypted = new Best_EncryptoClass();
bool Dialog::CheckConnectionToDB = false;
QString Dialog::username = "#";
QString Dialog::Access = "#";
QString Dialog::_DoctorNumber = "#";
QSqlDatabase Dialog::db = QSqlDatabase::addDatabase("QMYSQL");


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    bestReadXML *BRX = new bestReadXML();
    BRX->parseXML();
    db.setHostName(BRX->getHostname());
    db.setDatabaseName(BRX->getDatabasename());
    db.setUserName(BRX->getUsername());
    db.setPassword(BRX->getPassword());
    delete(BRX);
    if (db.open())
    {
        ui->ConnectionResultLabel->setText("Подключение к базе данных установлено.");
    }
    else
    {
        if (!CheckConnectionToDB)
        {
            if (db.lastError().text() == "Driver not loaded Driver not loaded")
            {
                QMessageBox::information(this, "Упс. Нежданчик", "Приложение не может подключится к серверу. "
                "Необходимо установить драйвер для подключения");
            }
            else
            {
                QMessageBox::information(this, "Упс. Нежданчик", "Приложение не может подключится к серверу. Ошибка -"
                " " + QString(db.lastError().text()));
            }
            ui->ConnectionResultLabel->setText("Не удается подключиться к базе данных.");
            CheckConnectionToDB = false;
            ui->AuthorizationButton->setEnabled(false);
        }
        else
        {
            QMessageBox::information(this, "Упс. Нежданчик", "Соединение с сервером внезапно прервалось. Проверьте свое интернет-соединение.");
        }
    }
}

void Dialog::on_AuthorizationButton_clicked()
{
    QString password;
    QSqlQuery qry(db);
    this->username = ui->LoginLine->text();
    password = Encrypted->encryptoString(ui->PasswordLine->text());
    qry.prepare("SELECT authorization.Doctor_Number, authorization.Login, authorization.Access_Level, doctors.First_Name, doctors.Last_Name, "
                "doctors.Patronymic FROM doctors INNER JOIN authorization ON authorization.Doctor_Number = "
                "doctors.Doctor_Number WHERE Login = :Login AND Password = :Password");
    qry.bindValue(":Login", this->username);
    qry.bindValue(":Password", password);
    qry.exec();
    if (qry.next())
    {
        CheckConnectionToDB = true;
        this->Access = qry.value(2).toString();
        _DoctorNumber = qry.value(0).toString();
        WF = new WorkForm();
        WF->SetDialogForm(this);
        WF->show();
        this->hide();
        QMessageBox::information(this, "Добро пожаловать", "Здравствуйте, " + qry.value(3).toString() + " " + qry.value(5).toString() + " " +
                                 qry.value(4).toString() + ".");
    }
    else
    {
        QMessageBox::information(this, "НЕ ОК", "Неверное имя пользователя или пароль.");
        qDebug() << qry.lastError();
    }
}

Dialog::~Dialog()
{
    delete ui;
}
