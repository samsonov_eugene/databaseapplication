#include "editpacientstableform.h"
#include "ui_editpacientstableform.h"

EditPacientsTableForm::EditPacientsTableForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPacientsTableForm)
{
    ui->setupUi(this);
    Dialog *D = new Dialog();
    db = D->Getdb();
    delete(D);
    WorkForm WF;
    ui->Birth_Sertificate_Number->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,15}"), this));
    ui->DeletePacient->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,15}"), this));
    ui->First_Name->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
    ui->Last_Name->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
    ui->Patronymic->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
}

EditPacientsTableForm::~EditPacientsTableForm()
{
    delete ui;
}

bool EditPacientsTableForm::CheckSpaceClick()
{
    bool ERROR = false;
    if ((ui->Last_Name->text() != "") && (ui->First_Name->text()!=""))
    {
        for (int i = 0; i < ui->First_Name->text().length(); i++)
            if (ui->First_Name->text().at(i) == ' ')
                ERROR = true;
        for (int i = 0; i < ui->Last_Name->text().length(); i++)
            if (ui->Last_Name->text().at(i) == ' ')
                ERROR = true;
        for (int i = 0; i < ui->Patronymic->text().length(); i++)
            if (ui->Patronymic->text().at(i) == ' ')
                ERROR = true;
    }
    else
        ERROR = true;

    if (ERROR)
        return false;
    else
        return true;
}

void EditPacientsTableForm::on_EditPacient_clicked()
{
    if ((!ui->First_Name->text().isEmpty()) && (!ui->Date_Of_Birth->text().isEmpty()) && (!ui->Last_Name->text().isEmpty())
            && (!ui->Address->text().isEmpty()) && (!ui->Phone_Number->text().isEmpty()) && (!ui->Birth_Sertificate_Number->text().isNull()))
    {
        QSqlQuery qry(db);
        QString Gender = "М";
        QDate CurDate;
        CurDate = CurDate.currentDate();
        if (ui->PacientMale->isChecked())
            Gender = "М";
        else
            Gender = "Ж";
        bool ERRORcheck = false;
        for (int i = 0; i < ui->Phone_Number->text().length() - 1; i++)
        {
            if (ui->Phone_Number->text().at(i) == "-")
                if (ui->Phone_Number->text().at(i+1) == "-")
                    ERRORcheck = true;
        }

        if (!ERRORcheck)
        {
            if (CheckSpaceClick())
            {
                if (ui->Date_Of_Birth->date() <= CurDate.currentDate())
                {
                    qry.prepare("UPDATE pacients SET Date_Of_Birth= :DOB, Gender = :G, First_Name = :FN, Last_Name = :LN, Patronymic = :P, "
                                "Address = :A, Phone_Number = :PN WHERE Policy_Number = :BSN");
                    qry.bindValue(":DOB", ui->Date_Of_Birth->text());
                    qry.bindValue(":G", Gender);
                    qry.bindValue(":FN", ui->First_Name->text());
                    qry.bindValue(":LN", ui->Last_Name->text());
                    qry.bindValue(":P", ui->Patronymic->text());
                    qry.bindValue(":A", ui->Address->text());
                    qry.bindValue(":PN", ui->Phone_Number->text());
                    qry.bindValue(":BSN", ui->Birth_Sertificate_Number->text());
                    if (qry.exec())
                    {
                        if (qry.numRowsAffected() != 1)
                            QMessageBox::information(this, "НЕ ОК", "Пациента с таким номером не существует");
                        else
                        {
                            WF->RefreshData();
                            QMessageBox::information(this,"ОК", "Пациент отредактирован");
                        }
                    }
                    else
                    {
                        QMessageBox::information(this,"НЕ ОК", "Произошла ошибка");
                        qDebug() << qry.lastError();
                    }
                }
                else
                    { QMessageBox::information(this,"НЕ ОК", "Дата рождения не может быть больше, чем сегодняшний день."); }
            }
            else
                { QMessageBox::information(this,"НЕ ОК", "В инициалах пациента не может быть пробелов."); }
        }
        else
        {
             QMessageBox::information(this,"НЕ ОК", "Проверьте правильность номера телефона");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void EditPacientsTableForm::on_AddPacient_clicked()
{
    if ((!ui->First_Name->text().isEmpty()) && (!ui->Date_Of_Birth->text().isEmpty()) && (!ui->Last_Name->text().isEmpty())
            && (!ui->Address->text().isEmpty()) && (!ui->Phone_Number->text().isEmpty()) && (!ui->Birth_Sertificate_Number->text().isNull()))
    {
        QSqlQuery qry(db);
        QString Gender = "М";
        QDate CurDate;
        CurDate = CurDate.currentDate();
        if (ui->PacientMale->isChecked())
        {
            Gender = "М";
        }
        else
        {
            Gender = "Ж";
        }
        bool ERRORcheck = false;
        for (int i = 0; i < ui->Phone_Number->text().length() - 1; i++)
        {
            if (ui->Phone_Number->text().at(i) == "-")
                if (ui->Phone_Number->text().at(i+1) == "-")
                    ERRORcheck = true;
        }
        if (!ERRORcheck)
        {
            if (CheckSpaceClick())
            {
                if (ui->Date_Of_Birth->date() <= CurDate.currentDate())
                {
                    qry.prepare("INSERT INTO pacients (Policy_Number, Date_Of_Birth, Gender, First_Name, Last_Name, Patronymic, Address, Phone_Number) "
                                "VALUES(:BSN, :DOB, :G, :FN, :LN, :P, :A, :PN)");
                    qry.bindValue(":DOB", ui->Date_Of_Birth->text());
                    qry.bindValue(":G", Gender);
                    qry.bindValue(":FN", ui->First_Name->text());
                    qry.bindValue(":LN", ui->Last_Name->text());
                    qry.bindValue(":P", ui->Patronymic->text());
                    qry.bindValue(":A", ui->Address->text());
                    qry.bindValue(":PN", ui->Phone_Number->text());
                    qry.bindValue(":BSN", ui->Birth_Sertificate_Number->text());
                    if (qry.exec())
                    {
                        WF->RefreshData();
                        QMessageBox::information(this,"ОК", "Пациент добавлен");
                    }
                    else
                    {
                        if (qry.lastError().number() == 1062)
                            QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Возможно, пациент с таким номером уже существует в базе");
                        else
                            QMessageBox::information(this,"НЕ ОК", "Во время добавления произошла ошибка");
                        qDebug() << qry.lastError();
                    }
                }
                else
                    { QMessageBox::information(this,"НЕ ОК", "Дата рождения не может быть больше, чем сегодняшний день."); }
            }
            else
                { QMessageBox::information(this,"НЕ ОК", "В инициалах пациента не может быть пробелов."); }
        }
        else
        { QMessageBox::information(this,"НЕ ОК", "Проверьте правильность номера телефона"); }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void EditPacientsTableForm::on_DeletePacientButton_clicked()
{
    if (!ui->DeletePacient->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("DELETE FROM pacients WHERE Policy_Number = :DP");
        qry.bindValue(":DP", ui->DeletePacient->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Пациента с таким номером сертификата не существует");
            else
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Пациент удален");
            }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Во время удаления произошла ошибка");
            qDebug() << qry.lastError();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили поле, необходимое для удаления.");
}

void EditPacientsTableForm::on_ExitToTableButton_clicked()
{
    this->WF->RefreshData();
    EditPacientsTableForm::hide();
}

void EditPacientsTableForm::on_Birth_Sertificate_Number_editingFinished()
{
}

