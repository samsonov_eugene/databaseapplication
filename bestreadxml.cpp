#include "bestreadxml.h"

void bestReadXML::parseXML()
{
    QFile *File = new QFile(":/xml/resources/bestxmlfileintheworld.xml");
    if (!File->open(QIODevice::ReadOnly))
        qDebug() << "Файл не может открыться.";
    else
    {
        QXmlStreamReader xml(File);
        for (int i = 0; i < 4; i++)
            dataArray[i] = "#####";

        while(xml.readNextStartElement())
        {
             if(xml.name() == "hostname")
             {
                  dataArray[0] = xml.readElementText();
             }

             else if(xml.name() == "databasename")
             {
                 dataArray[1] = xml.readElementText();
             }

             else if(xml.name() == "username")
             {
                 dataArray[2] = xml.readElementText();
             }

             else if(xml.name() == "password")
             {
                 dataArray[3] = xml.readElementText();
             }
        }
    }
}

void bestReadXML::parseKeyXML()
{
    QFile *File = new QFile(":/xml/resources/bestkey.xml");
    if (!File->open(QIODevice::ReadOnly))
        qDebug() << "Файл не может открыться.";
    else
    {
        QXmlStreamReader xml(File);
        while(xml.readNextStartElement())
        {
             if(xml.name() == "key")
             {
                key = xml.readElementText().toLocal8Bit();
             }
        }
    }
}
