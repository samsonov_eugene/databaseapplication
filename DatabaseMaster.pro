#-------------------------------------------------
#
# Project created by QtCreator 2017-11-30T20:55:23
#
#-------------------------------------------------

QT       += core gui
QT       += printsupport
QT       += sql
QT       += xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DatabaseMaster
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        dialog.cpp \
    workform.cpp \
    editdoctableform.cpp \
    editpacientstableform.cpp \
    editappointmentsform.cpp \
    best_encryptoclass.cpp \
    bestreadxml.cpp

HEADERS += \
        dialog.h \
    workform.h \
    editdoctableform.h \
    editpacientstableform.h \
    editappointmentsform.h \
    best_encryptoclass.h \
    bestreadxml.h

FORMS += \
        dialog.ui \
    workform.ui \
    editdoctableform.ui \
    editpacientstableform.ui \
    editappointmentsform.ui

#LIBS += -L\sqldrivers\qsqlmysql.dll -library
#LIBS += -L\sqldrivers\libmysql.dll -library

win32: LIBS += -L$$PWD/'../../Program Files/MySQL/MySQL Connector C++ 1.1.9/lib/opt/' -lmysqlcppconn

INCLUDEPATH += $$PWD/'../../Program Files/MySQL/MySQL Connector C++ 1.1.9/lib/opt'
DEPENDPATH += $$PWD/'../../Program Files/MySQL/MySQL Connector C++ 1.1.9/lib/opt'

DISTFILES +=

RESOURCES += \
    resource.qrc \
    xmlresources.qrc
