#ifndef EDITPACIENTSTABLEFORM_H
#define EDITPACIENTSTABLEFORM_H

#include <QDialog>
#include "dialog.h"
#include "workform.h"

namespace Ui {
class EditPacientsTableForm;
}
class WorkForm;

class EditPacientsTableForm : public QDialog
{
    Q_OBJECT

public:

    void *SetWorkForm(WorkForm *dio)
    { this->WF = dio; }

    explicit EditPacientsTableForm(QWidget *parent = 0);
    bool CheckSpaceClick();
    ~EditPacientsTableForm();

private slots:
    void on_EditPacient_clicked();

    void on_AddPacient_clicked();

    void on_DeletePacientButton_clicked();

    void on_ExitToTableButton_clicked();

    void on_Birth_Sertificate_Number_editingFinished();

private:
    Ui::EditPacientsTableForm *ui;
    QSqlDatabase db;
    WorkForm *WF;
};

#endif // EDITPACIENTSTABLEFORM_H
