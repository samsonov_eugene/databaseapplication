#ifndef EDITAPPOINTMENTSFORM_H
#define EDITAPPOINTMENTSFORM_H

#include <QDialog>
#include "workform.h"
#include "dialog.h"
#include <QPrinter>
#include <QPrintDialog>
#include <QVector>
#include <QTextEdit>

namespace Ui {
class EditAppointmentsForm;
}
class EditAppointmentsForm : public QDialog
{
    Q_OBJECT

public:

    void *SetWorkForm(WorkForm *dio)
    { this->WF = dio; }

    explicit EditAppointmentsForm(QWidget *parent = 0);

    void addDataToDoctypebox();
    void addDataToDocnumberbox();
    void getDocNumber();
    void addDataToDocTimebox();
    QString showHTMLPrintSource(const QString &Number,
                             const QString &PolicyNumber,
                             const QString &pacient,
                             const QString &Doc,
                             const QString &Specialisation,
                             const QString &Data,
                             const QString &time,
                             const QString &CabNum);

    ~EditAppointmentsForm();

private slots:
    void on_EditAppointmentButton_clicked();

    void on_AddAppointment_Button_clicked();

    void on_DeleteAppointmentButton_clicked();

    void on_exitButton_clicked();

    void on_DocType_box_activated(const QString &arg1);

    void on_DocNumber_box_activated(const QString &arg1);

    void on_Birth_Certificate_Num_editingFinished();

    void on_Appointment_Date_editingFinished();

    void on_printButton_clicked();

private:
    WorkForm *WF;
    Ui::EditAppointmentsForm *ui;
    QSqlDatabase db;
    static QString CurrentDocNumber;
};

#endif // EDITAPPOINTMENTSFORM_H
