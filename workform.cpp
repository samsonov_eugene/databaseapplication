#include "workform.h"
#include "ui_workform.h"
#include <QTimer>
#include <QRegExpValidator>

WorkForm::WorkForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WorkForm)
{
    ui->setupUi(this);
    this->RefreshData();
    if (D->GetAccess() == "Глав. врач")
    {
        ui->DBData_Table->removeTab(3);
        ui->DBData_Table->removeTab(3);
        ui->DBData_Table->removeTab(4);
    }
    else if (D->GetAccess() == "Врач")
    {
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(1);
    }
    else if (D->GetAccess() == "Регистратор")
    {
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(0);
        ui->DBData_Table->removeTab(2);
    }
    /***/
    ui->userIDLine->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->DeleteLine->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->PositinNumber->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->PosDel->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    Encrypted = D->getEncrypted();
    /***/
    ui->DBData_Table->setCurrentIndex(0);
}

void WorkForm::AddDataToDocViewTable()
{
    QSqlQuery qry(db);
    qry.prepare("SELECT doctors.Doctor_Number, positions.Position, doctors.First_Name, doctors.Last_Name, cabinets.Cabinet_Number, doctors.Phone_Number, "
                "working_hours.Start_Time, working_hours.End_Time, vacations.Start_Date, vacations.End_Date "
                "FROM doctors "
                "INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                "INNER JOIN cabinets ON doctors.Doctor_Number = cabinets.Doctor_Number "
                "INNER JOIN vacations ON vacations.Doctor_Number = doctors.Doctor_Number "
                "INNER JOIN working_hours ON working_hours.Doctor_Number = doctors.Doctor_Number "
                "WHERE positions.Position != 'Регистратор'");
    if (qry.exec())
    {
        DocViewData.setQuery(qry);
        DocViewData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер врача"));
        DocViewData.setHeaderData(1,Qt::Horizontal,QObject::tr("Специализация"));
        DocViewData.setHeaderData(2,Qt::Horizontal,QObject::tr("Имя"));
        DocViewData.setHeaderData(3,Qt::Horizontal,QObject::tr("Фамилия"));
        DocViewData.setHeaderData(4,Qt::Horizontal,QObject::tr("Номер кабинета"));
        DocViewData.setHeaderData(5,Qt::Horizontal,QObject::tr("Номер телефона"));
        DocViewData.setHeaderData(6,Qt::Horizontal,QObject::tr("Время начала работы"));
        DocViewData.setHeaderData(7,Qt::Horizontal,QObject::tr("Время окончания работы"));
        DocViewData.setHeaderData(8,Qt::Horizontal,QObject::tr("Дата начала отпуска"));
        DocViewData.setHeaderData(9,Qt::Horizontal,QObject::tr("Дата окончания отпуска"));
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&DocViewData);
        ui->tableDocView->setModel(m);
        ui->tableDocView->setSortingEnabled(true);
    }
    else
    {
        qDebug() << qry.lastError();
    }
}

void WorkForm::AddDataToAppointmentsViewTable()
{
    QSqlQuery qry(db);
    Dialog *D;
    qry.prepare("SELECT appointments.Appointment_Number, pacients.First_Name, pacients.Patronymic, pacients.Last_Name, cabinets.Cabinet_Number, "
                "appointments.Date_Of_Receipt, appointments.Time_Of_Receipt FROM appointments "
                "INNER JOIN doctors ON appointments.Doctor_Number = doctors.Doctor_Number "
                "INNER JOIN cabinets ON cabinets.Doctor_Number = doctors.Doctor_Number "
                "INNER JOIN pacients ON appointments.Policy_Number = pacients.Policy_Number "
                "WHERE doctors.Doctor_Number = :DDN");
    qry.bindValue(":DDN", D->DoctorNumber());
    if (!qry.exec())
    {
        qDebug() << qry.lastError();
    }
    else
    {
        AppointmentsViewData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&AppointmentsViewData);
        ui->AppointmentsTableView->setModel(m);
        ui->AppointmentsTableView->setSortingEnabled(true);
        AppointmentsViewData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер записи"));
        AppointmentsViewData.setHeaderData(1,Qt::Horizontal,QObject::tr("Имя пациента"));
        AppointmentsViewData.setHeaderData(2,Qt::Horizontal,QObject::tr("Отчество пациента"));
        AppointmentsViewData.setHeaderData(3,Qt::Horizontal,QObject::tr("Фамилия пациента"));
        AppointmentsViewData.setHeaderData(4,Qt::Horizontal,QObject::tr("Номер кабинета"));
        AppointmentsViewData.setHeaderData(5,Qt::Horizontal,QObject::tr("Дата приема"));
        AppointmentsViewData.setHeaderData(6,Qt::Horizontal,QObject::tr("Время приема"));
    }
}

void WorkForm::RefreshData()
{
    AddDataToDocTable();
    AddDataToFirstTable();
    AddDataToPacientsTable();
    AddDataToPositionsTable();
    AddDataToAppointmentsTable();
    AddDataToAppointmentsViewTable();
}

void WorkForm::AddDataToAppointmentsTable()
{
    QSqlQuery qry(db);
    if (!qry.exec("SELECT Appointment_Number, Policy_Number, Doctor_Number, Date_Of_Receipt, Time_Of_Receipt FROM appointments"))
    {
        qDebug() << qry.lastError();
    }
    else
    {
        AppointmentsData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&AppointmentsData);
        ui->AppointmentsTable->setModel(m);
        ui->AppointmentsTable->setSortingEnabled(true);
        AppointmentsData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер записи"));
        AppointmentsData.setHeaderData(1,Qt::Horizontal,QObject::tr("№ полиса"));
        AppointmentsData.setHeaderData(2,Qt::Horizontal,QObject::tr("Номер врача"));
        AppointmentsData.setHeaderData(3,Qt::Horizontal,QObject::tr("Дата приема"));
        AppointmentsData.setHeaderData(4,Qt::Horizontal,QObject::tr("Время приема"));
    }
}

void WorkForm::AddDataToPositionsTable()
{
    QSqlQuery qry(db);
    if (!qry.exec("SELECT Position_Number, Position FROM positions"))
    {
        qDebug() << qry.lastError();
    }
    else
    {
        PositionsData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&PositionsData);
        ui->PositionsTable->setModel(m);
        ui->PositionsTable->setSortingEnabled(true);
        PositionsData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер специализации"));
        PositionsData.setHeaderData(1,Qt::Horizontal,QObject::tr("Специализация"));
    }
}

void WorkForm::AddDataToPacientsTable()
{
    QSqlQuery qry(db);
    if (!qry.exec("SELECT Policy_Number, Date_Of_Birth, Gender, First_Name, Patronymic, Last_Name, Address, Phone_Number FROM pacients"))
    {
        qDebug() << qry.lastError();
    }
    else
    {
        PacientsData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&PacientsData);
        ui->Pacients_View->setModel(m);
        ui->Pacients_View->setSortingEnabled(true);
        PacientsData.setHeaderData(0,Qt::Horizontal,QObject::tr("№ полиса"));
        PacientsData.setHeaderData(1,Qt::Horizontal,QObject::tr("Дата рождения"));
        PacientsData.setHeaderData(2,Qt::Horizontal,QObject::tr("Пол"));
        PacientsData.setHeaderData(3,Qt::Horizontal,QObject::tr("Имя"));
        PacientsData.setHeaderData(4,Qt::Horizontal,QObject::tr("Отчество"));
        PacientsData.setHeaderData(5,Qt::Horizontal,QObject::tr("Фамилия"));
        PacientsData.setHeaderData(6,Qt::Horizontal,QObject::tr("Адрес"));
        PacientsData.setHeaderData(7,Qt::Horizontal,QObject::tr("Номер телефона"));
    }
}

void WorkForm::on_AddUserButton_clicked()
{
    if ((!ui->userIDLine->text().isEmpty()) && (!ui->usernameLine->text().isEmpty()) && (!ui->passwordLine->text().isEmpty()))
    {
        QSqlQuery qry(db);
        QSqlQuery checkRole(db);
        QString Role;
        QString Password = Encrypted->encryptoString(ui->passwordLine->text());
        checkRole.prepare("SELECT doctors.Doctor_Number, positions.Position FROM doctors INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                    "WHERE doctors.Doctor_Number = :S");
        checkRole.bindValue(":S", ui->userIDLine->text());
        checkRole.exec();
        checkRole.first();
        if (checkRole.value(1).toString() == "Регистратор") { Role = "Регистратор"; }
        else if (checkRole.value(1).toString() == "Глав. врач") { Role = "Глав. врач"; }
        else { Role = "Врач"; }
        qry.prepare("INSERT INTO authorization (Doctor_Number, Login, Password, Access_Level) VALUES(:UIL, :UL, :PL, :R)");
        qry.bindValue(":UIL", ui->userIDLine->text());
        qry.bindValue(":UL", ui->usernameLine->text());
        qry.bindValue(":PL", Password);
        qry.bindValue(":R", Role);
        if (qry.exec())
        {
            this->RefreshData();
            QMessageBox::information(this, "OK", "Аккаунт успешно добавлен");
        }
        else if (qry.lastError().number() == 1452)
        {
            QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Такого врача не существует в списке врачей");
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Убедитесь, что данный псевдоним не используется и убедитесь, что у этого врача отсутствует аккаунт");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

WorkForm::~WorkForm()
{
    delete ui;
}

void WorkForm::AddDataToDocTable()
{
    QSqlQuery qry(db);
    if (!qry.exec("SELECT doctors.Doctor_Number, positions.Position, doctors.Gender, "
                  "doctors.First_Name, doctors.Patronymic, doctors.Last_Name, doctors.Phone_Number, "
                  "cabinets.Cabinet_Number, working_hours.Start_Time, working_hours.End_Time, vacations.Start_Date, vacations.End_Date "
                  "FROM doctors "
                  "INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                  "INNER JOIN cabinets ON doctors.Doctor_Number = cabinets.Doctor_Number "
                  "INNER JOIN vacations ON vacations.Doctor_Number = doctors.Doctor_Number "
                  "INNER JOIN working_hours ON working_hours.Doctor_Number = doctors.Doctor_Number "))
    {
        qDebug() << qry.lastError();
    }
    else
    {
        DocData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&DocData);
        ui->DocShow->setModel(m);
        ui->DocShow->setSortingEnabled(true);
        DocData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер врача"));
        DocData.setHeaderData(1,Qt::Horizontal,QObject::tr("Специализация"));
        DocData.setHeaderData(2,Qt::Horizontal,QObject::tr("Пол"));
        DocData.setHeaderData(3,Qt::Horizontal,QObject::tr("Имя"));
        DocData.setHeaderData(4,Qt::Horizontal,QObject::tr("Отчество"));
        DocData.setHeaderData(5,Qt::Horizontal,QObject::tr("Фамилия"));
        DocData.setHeaderData(6,Qt::Horizontal,QObject::tr("Номер телефона"));
        DocData.setHeaderData(7,Qt::Horizontal,QObject::tr("Номер кабинета"));
        DocData.setHeaderData(8,Qt::Horizontal,QObject::tr("Время начала работы"));
        DocData.setHeaderData(9,Qt::Horizontal,QObject::tr("Время окончания работы"));
        DocData.setHeaderData(10,Qt::Horizontal,QObject::tr("Дата начала отпуска"));
        DocData.setHeaderData(11,Qt::Horizontal,QObject::tr("Дата окончания отпуска"));
    }
}

void WorkForm::AddDataToFirstTable()
{
    QSqlQuery qry(db);
    if (!qry.exec("SELECT Doctor_Number, Login, Access_Level FROM authorization"))
    {
        qDebug() << qry.lastError();
    }
    else
    {
        DocAccountsData.setQuery(qry);
        QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
        m->setDynamicSortFilter(true);
        m->setSourceModel(&DocAccountsData);
        ui->DocAccountsShow->setModel(m);
        ui->DocAccountsShow->setSortingEnabled(true);
        DocAccountsData.setHeaderData(0,Qt::Horizontal,QObject::tr("Номер врача"));
        DocAccountsData.setHeaderData(1,Qt::Horizontal,QObject::tr("Псевдоним"));
        DocAccountsData.setHeaderData(2,Qt::Horizontal,QObject::tr("Уровень доступа"));
    }
}

void WorkForm::on_DeleteButton_clicked()
{
    if (!ui->DeleteLine->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("DELETE FROM authorization WHERE Doctor_Number = :DL");
        qry.bindValue(":DL", ui->DeleteLine->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Врача с таким ID не существует");
            else
            {
                this->RefreshData();
                QMessageBox::information(this,"ОК", "Врач был успешно удален");
            }
        } else QMessageBox::information(this,"НЕ ОК", "При удалении произошла ошибка");
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили поле, необходимое для удаления.");
}

void WorkForm::on_EditUserButton_clicked()
{
    if ((!ui->userIDLine->text().isEmpty()) && (!ui->usernameLine->text().isEmpty()) && (!ui->passwordLine->text().isEmpty()))
    {
        QSqlQuery qry(db);
        QSqlQuery checkRole(db);
        QString Password = Encrypted->encryptoString(ui->passwordLine->text());
        QString Role;
        checkRole.prepare("SELECT doctors.Doctor_Number, positions.Position FROM doctors INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                    "WHERE doctors.Doctor_Number = :S");
        checkRole.bindValue(":S", ui->userIDLine->text());
        checkRole.exec();
        checkRole.first();
        if (checkRole.value(1).toString() == "Регистратор") { Role = "Регистратор"; }
        else if (checkRole.value(1).toString() == "Глав. врач") { Role = "Глав. врач"; }
        else { Role = "Врач"; }
        qry.prepare("UPDATE authorization SET Login = :UL, Password = :PL, Access_Level = :R WHERE Doctor_Number = :UIL");
        qry.bindValue(":UIL", ui->userIDLine->text());
        qry.bindValue(":UL", ui->usernameLine->text());
        qry.bindValue(":PL", Password);
        qry.bindValue(":R", Role);
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this,"ОК", "Произошла ошибка. Возможно, врача с таким ID не существует или этот логин уже занят");
            else
            {
                this->RefreshData();
                QMessageBox::information(this,"ОК", "Редактирование прошло успешно");
            }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "При редактировании произошла ошибка");
        }
    }
    else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void WorkForm::on_DBData_Table_tabBarClicked(int index)
{
    Dialog *D;
    if (D->GetAccess() == "Глав. врач")
    {
        if (index == 0)
            AddDataToFirstTable();
        else if (index == 1)
            AddDataToDocTable();
        else if (index == 2)
            AddDataToPositionsTable();
        else if (index == 3)
            AddDataToAppointmentsViewTable();
    }
    else if (D->GetAccess() == "Врач")
    {
        if (index == 0)
            AddDataToAppointmentsViewTable();
    }
    else if (D->GetAccess() == "Регистратор")
    {
        if (index == 0)
            AddDataToPacientsTable();
        else if (index == 1)
            AddDataToAppointmentsTable();
        else if (index == 2)
            AddDataToDocViewTable();
    }
}

void WorkForm::on_EditDocTable_clicked()
{
    EditDocTableForm *ETF = new EditDocTableForm();
    ETF->SetWorkForm(D->GetWorkForm());
    ETF->show();
}

void WorkForm::on_Edit_Pacients_Button_clicked()
{
    EditPacientsTableForm *EPTF = new EditPacientsTableForm();
    EPTF->SetWorkForm(D->GetWorkForm());
    EPTF->show();
}

void WorkForm::on_EditPos_clicked()
{
    QSqlQuery qry(db);
    if ((!ui->Position->text().isEmpty()) && (!ui->Position->text().isEmpty()))
    {
        qry.prepare("UPDATE positions SET Position = :P WHERE Position_Number = :PN");
        qry.bindValue(":P", ui->Position->text());
        qry.bindValue(":PN", ui->PositinNumber->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this,"ОК", "Произошла ошибка. Возможно, что специализации с таким номером не существует");
            else
            {
                this->RefreshData();
                QMessageBox::information(this,"ОК", "Редактирование прошло успешно");
            }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "При редактировании произошла ошибка");
        }
    }
    else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void WorkForm::on_AddPos_clicked()
{
    if ((!ui->Position->text().isEmpty()) && (!ui->Position->text().isEmpty()))
    {
        QSqlQuery qry(db);
        qry.prepare("INSERT INTO positions (Position_Number, Position) VALUES(:PN, :P)");
        qry.bindValue(":P", ui->Position->text());
        qry.bindValue(":PN", ui->PositinNumber->text());
        if (qry.exec())
        {
            this->RefreshData();
            QMessageBox::information(this, "OK", "Специализация успешно добавлена");
        }
        else
        {
            if (qry.lastError().number() == 1062)
                QMessageBox::information(this,"НЕ ОК", "Специализация с таким номером уже существует");
            else
                QMessageBox::information(this,"НЕ ОК", "Произошла ошибка");
            qDebug() << qry.lastError().text();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void WorkForm::on_DeletePos_clicked()
{
    if (!ui->PosDel->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("DELETE FROM positions WHERE Position_Number = :PD");
        qry.bindValue(":PD", ui->PosDel->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Произошла ошибка. Возможно, что специализации с таким номером не существует");
            else
            {
                this->RefreshData();
                QMessageBox::information(this,"ОК", "Специализация успешно удалена");
            }
        }
        else
            QMessageBox::information(this,"НЕ ОК", "При удалении произошла ошибка");
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили поле, необходимое для удаления");
}

void WorkForm::on_EditAppointmentsTableButtonEditAppointmentsTableButton_clicked()
{
    EditAppointmentsForm *EPF = new EditAppointmentsForm();
    EPF->SetWorkForm(D->GetWorkForm());
    EPF->show();
}

void WorkForm::on_SearchDocTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT doctors.Doctor_Number, positions.Position, doctors.Gender, "
                        "doctors.First_Name, doctors.Patronymic, doctors.Last_Name, doctors.Phone_Number, "
                        "cabinets.Cabinet_Number, working_hours.Start_Time, working_hours.End_Time, vacations.Start_Date, vacations.End_Date "
                        "FROM doctors "
                        "INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                        "INNER JOIN cabinets ON doctors.Doctor_Number = cabinets.Doctor_Number "
                        "INNER JOIN vacations ON vacations.Doctor_Number = doctors.Doctor_Number "
                        "INNER JOIN working_hours ON working_hours.Doctor_Number = doctors.Doctor_Number "
                        "WHERE ((doctors.Doctor_Number = :S) or (positions.Position = :S) or (doctors.Gender = :S) or "
                        "(doctors.First_Name = :S) or (doctors.Patronymic = :S) or (doctors.Last_Name = :S) or (doctors.Phone_Number = :S) or (cabinets.Cabinet_Number = :S) or "
                        "(working_hours.Start_Time = STR_TO_DATE(:S, '%H:%i')) or (working_hours.End_Time = STR_TO_DATE(:S, '%H:%i')) or "
                        "(vacations.Start_Date = STR_TO_DATE(:S, '%d.%m.%Y')) or (vacations.End_Date = STR_TO_DATE(:S, '%d.%m.%Y')))");
    searchQuery.bindValue(":S", ui->SearchDocTableEdit->text());
    qDebug() << searchQuery.lastError().text();
    searchQuery.exec();
    if (searchQuery.first())
    {
       DocData.setQuery(searchQuery);
       ui->DocShow->setModel(&DocData);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
}

void WorkForm::on_SearchPositionsButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT * FROM positions WHERE (Position_Number= :S) or (Position= :S)");
    searchQuery.bindValue(":S", ui->SearchPositionsEdit->text());
    searchQuery.exec();
    if (searchQuery.first())
    {
       PositionsData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&PositionsData);
       ui->PositionsTable->setModel(m);
       ui->PositionsTable->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
}

void WorkForm::on_SearchPacientsTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT * FROM pacients WHERE (Policy_Number= :S) or (Date_Of_Birth = STR_TO_DATE(:S, '%d.%m.%Y')) or (Gender = :S) or (First_Name= :S)"
                        " or (Last_Name = :S) or (Patronymic = :S) or (Address = :S) or (Phone_Number = :S)");
    searchQuery.bindValue(":S", ui->SearchPacientsTableEdit->text());
    searchQuery.exec();
    if (searchQuery.first())
    {
       PacientsData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&PacientsData);
       ui->Pacients_View->setModel(m);
       ui->Pacients_View->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
    qDebug() << ui->SearchPacientsTableEdit->text() << " " << searchQuery.lastError() << " " << searchQuery.value(0);
}

void WorkForm::on_SearchAppointmentsTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT Appointment_Number, Policy_Number, Doctor_Number, Date_Of_Receipt, Time_Of_Receipt FROM appointments "
                        "WHERE (Appointment_Number = :S) or (Policy_Number = :S) or (Doctor_Number = :S) or (Date_Of_Receipt = STR_TO_DATE(:S, '%d.%m.%Y')) or (Time_Of_Receipt = STR_TO_DATE(:S, '%H:%i'))");
    searchQuery.bindValue(":S", ui->SearchAppointmentsTableEdit->text());
    searchQuery.exec();
    if (searchQuery.first())
    {
       AppointmentsData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&AppointmentsData);
       ui->AppointmentsTable->setModel(m);
       ui->AppointmentsTable->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
    qDebug() << ui->SearchAppointmentsTableEdit->text() << " " << searchQuery.lastError() << " " << searchQuery.value(0);
}

void WorkForm::on_SearchAppointmentsViewTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT appointments.Appointment_Number, pacients.First_Name, pacients.Last_Name, pacients.Patronymic, cabinets.Cabinet_Number, "
                        "appointments.Date_Of_Receipt, appointments.Time_Of_Receipt FROM appointments INNER JOIN doctors ON appointments.Doctor_Number = doctors.Doctor_Number "
                        "INNER JOIN cabinets ON cabinets.Doctor_Number = doctors.Doctor_Number "
                        "INNER JOIN pacients ON appointments.Policy_Number = pacients.Policy_Number "
                        "WHERE ((doctors.Doctor_Number = :DDN) AND ((appointments.Appointment_Number = :S) or (pacients.First_Name = :S) or (pacients.Last_Name = :S) or (pacients.Patronymic = :S) "
                        "or (cabinets.Cabinet_Number = :S) or (appointments.Date_Of_Receipt = STR_TO_DATE(:S, '%d.%m.%Y')) or (appointments.Time_Of_Receipt = STR_TO_DATE(:S, '%H:%i'))))");
    searchQuery.bindValue(":S", ui->SearchAppointmentsViewTableEdit->text());
    searchQuery.bindValue(":DDN", D->DoctorNumber());
    searchQuery.exec();
    if (searchQuery.first())
    {
       AppointmentsViewData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&AppointmentsViewData);
       ui->AppointmentsTableView->setModel(m);
       ui->AppointmentsTableView->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
    qDebug() << ui->SearchAppointmentsViewTableEdit->text() << " " << searchQuery.lastError() << " " << searchQuery.value(0);
}

void WorkForm::on_SearchDocViewTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT doctors.Doctor_Number, positions.Position, doctors.First_Name, doctors.Last_Name, cabinets.Cabinet_Number, doctors.Phone_Number, "
                        "working_hours.Start_Time, working_hours.End_Time, vacations.Start_Date, vacations.End_Date "
                        "FROM doctors "
                        "INNER JOIN positions ON doctors.Position_Number = positions.Position_Number "
                        "INNER JOIN cabinets ON doctors.Doctor_Number = cabinets.Doctor_Number "
                        "INNER JOIN vacations ON vacations.Doctor_Number = doctors.Doctor_Number "
                        "INNER JOIN working_hours ON working_hours.Doctor_Number = doctors.Doctor_Number "
                        "WHERE (positions.Position != 'Регистратор') and ((positions.Position = :S) or (doctors.Doctor_Number = :S) or (doctors.First_Name = :S) "
                        "or (doctors.Last_Name = :S) or (cabinets.Cabinet_Number = :S) or (doctors.Phone_Number = :S) or (cabinets.Cabinet_Number = :S) or "
                        "(working_hours.Start_Time = STR_TO_DATE(:S, '%H:%i')) or (working_hours.End_Time = STR_TO_DATE(:S, '%H:%i')) or "
                        "(vacations.Start_Date = STR_TO_DATE(:S, '%d.%m.%Y')) or (vacations.End_Date = STR_TO_DATE(:S, '%d.%m.%Y')))");
    searchQuery.bindValue(":S", ui->SearchDocViewTableEdit->text());
    searchQuery.exec();
    if (searchQuery.first())
    {
       DocViewData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&DocViewData);
       ui->tableDocView->setModel(&DocViewData);
       ui->tableDocView->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
    qDebug() << ui->SearchDocViewTableEdit->text() << " " << searchQuery.lastError() << " " << searchQuery.value(0);
}

void WorkForm::on_SearchAccountsTableButton_clicked()
{
    QSqlQuery searchQuery;
    searchQuery.prepare("SELECT * FROM authorization WHERE (Doctor_Number= :S) or (Login = :S) "
                 "or (Password = :S) or (Access_Level= :S)");
    searchQuery.bindValue(":S", ui->SearchAccountsTableEdit->text());
    searchQuery.exec();
    if (searchQuery.first())
    {
       DocAccountsData.setQuery(searchQuery);
       QSortFilterProxyModel *m = new QSortFilterProxyModel(this);
       m->setDynamicSortFilter(true);
       m->setSourceModel(&DocAccountsData);
       ui->DocAccountsShow->setModel(m);
       ui->DocAccountsShow->setSortingEnabled(true);
    }
    else
    { QMessageBox::information(this,"НЕ ОК", "Совпадений не найдено."); }
}

void WorkForm::on_AutisticsDevsCopy_linkActivated(const QString &link)
{
}
