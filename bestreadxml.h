#ifndef BESTREADXML_H
#define BESTREADXML_H
#include <QXmlStreamReader>
#include <QFile>
#include <QDebug>

class bestReadXML
{
private:
    QString dataArray[4];
    QByteArray key;

public:
    QString getHostname()
    { return dataArray[0]; }

    QString getDatabasename()
    { return dataArray[1]; }

    QString getUsername()
    { return dataArray[2]; }

    QString getPassword()
    { return dataArray[3]; }

    QByteArray getKey()
    { return key; }

    void parseXML();
    void parseKeyXML();
};

#endif // BESTREADXML_H
