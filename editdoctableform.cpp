#include "editdoctableform.h"
#include "ui_editdoctableform.h"

EditDocTableForm::EditDocTableForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDocTableForm)
{
    ui->setupUi(this);
    Dialog *D = new Dialog();
    db = D->Getdb();
    ui->Doc_Number->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->Position_Number->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->Doc_Number_Cab->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->DelNumEdit->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->DocNumTime->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->DocNumVac->setValidator(new QRegExpValidator(QRegExp("[1-9]\\d{0,5}"), this));
    ui->First_Name->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
    ui->Last_Name->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
    ui->Patronymic->setValidator(new QRegExpValidator(QRegExp("[А-Я][а-я]{1,21}")));
}

EditDocTableForm::~EditDocTableForm()
{
    delete ui;
}

bool EditDocTableForm::CheckSpaceClick()
{
    bool ERROR = false;
    if ((ui->Last_Name->text() != "") && (ui->First_Name->text()!=""))
    {
        for (int i = 0; i < ui->First_Name->text().length(); i++)
            if (ui->First_Name->text().at(i) == ' ')
                ERROR = true;
        for (int i = 0; i < ui->Last_Name->text().length(); i++)
            if (ui->Last_Name->text().at(i) == ' ')
                ERROR = true;
        for (int i = 0; i < ui->Patronymic->text().length(); i++)
            if (ui->Patronymic->text().at(i) == ' ')
                ERROR = true;
    }
    else
        ERROR = true;

    if (ERROR)
        return false;
    else
        return true;
}

void EditDocTableForm::on_AddButton_clicked()
{
    if ((!ui->Position_Number->text().isEmpty()) && (!ui->First_Name->text().isEmpty())&&
            (!ui->Last_Name->text().isEmpty()) && (!ui->PhoneNumber->text().isEmpty()))
    {
        QSqlQuery qry(db);
        QString Gender = "М";
        if (ui->DocFemale->isChecked())
            Gender = "Ж";
        else
            Gender = "М";

        bool ERRORcheck = false;
        for (int i = 0; i < ui->PhoneNumber->text().length() - 1; i++)
        {
            if (ui->PhoneNumber->text().at(i) == "-")
                if (ui->PhoneNumber->text().at(i+1) == "-")
                    ERRORcheck = true;
        }

        if (!ERRORcheck)
        {
            if (CheckSpaceClick())
            {
                qry.prepare("INSERT INTO doctors (Doctor_Number, Position_Number, First_Name, Last_Name, Patronymic, Phone_number, Gender) VALUES("
                            ":DN, :PN, :FN, :LN, :Patr, :PhN, :G)");
                qry.bindValue(":DN", ui->Doc_Number->text());
                qry.bindValue(":PN", ui->Position_Number->text());
                qry.bindValue(":FN", ui->First_Name->text());
                qry.bindValue(":LN", ui->Last_Name->text());
                qry.bindValue(":Patr", ui->Patronymic->text());
                qry.bindValue(":PhN", ui->PhoneNumber->text());
                qry.bindValue(":G", Gender);

                if (qry.exec())
                {
                    if (qry.numRowsAffected() != 1)
                    {
                       QMessageBox::information(this,"НЕ ОК", "Такой врач существует");
                    }
                    else
                    {
                        QMessageBox::information(this,"ОК", "Врач добавлен");
                        this->WF->RefreshData();
                    }
                }
                else
                {
                    if (qry.lastError().number() == 1062)
                        QMessageBox::information(this,"НЕ ОК", "Врач с таким номером уже существует");
                    else if (qry.lastError().number() == 1452)
                        QMessageBox::information(this,"НЕ ОК", "Такой должности не существует");
                    else
                        QMessageBox::information(this,"НЕ ОК", "Во время добавления произошла ошибка");
                    qDebug() << qry.lastError();
                }
            }
            else
                { QMessageBox::information(this,"НЕ ОК", "В инициалах врача не может быть пробелов."); }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Проверьте правильность номера");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void EditDocTableForm::on_EditButton_clicked()
{
    if ((!ui->Doc_Number->text().isEmpty()) && (!ui->Position_Number->text().isEmpty()) && (!ui->First_Name->text().isEmpty()) &&
            (!ui->Last_Name->text().isEmpty()) && (!ui->PhoneNumber->text().isEmpty()))
    {
        QSqlQuery qry(db);
        QString Gender = "М";
        if (ui->DocFemale->isChecked())
        {
            Gender = "Ж";
        }
        else
        {
            Gender = "М";
        }
        bool ERRORcheck = false;
        for (int i = 0; i < ui->PhoneNumber->text().length() - 1; i++)
        {
            if (ui->PhoneNumber->text().at(i) == "-")
                if (ui->PhoneNumber->text().at(i+1) == "-")
                    ERRORcheck = true;
        }

        if (!ERRORcheck)
        {
            if (CheckSpaceClick())
            {
                qry.prepare("UPDATE doctors SET Position_Number= :PN, Gender= :G, First_Name= :FN, Last_Name= "
                            ":LN, Patronymic= :Patr, Phone_Number = :PhN WHERE Doctor_Number = :DN");
                qry.bindValue(":DN", ui->Doc_Number->text());
                qry.bindValue(":PN", ui->Position_Number->text());
                qry.bindValue(":FN", ui->First_Name->text());
                qry.bindValue(":LN", ui->Last_Name->text());
                qry.bindValue(":Patr", ui->Patronymic->text());
                qry.bindValue(":PhN", ui->PhoneNumber->text());
                qry.bindValue(":G", Gender);
                if (qry.exec())
                {
                    if (qry.numRowsAffected() != 1)
                    {
                        QMessageBox::information(this,"ОК", "Такого врача не существует");
                    }
                    else
                    {
                        this->WF->RefreshData();
                        QMessageBox::information(this,"ОК", "Врач отредактирован");
                    }

                }
                else
                {
                    QMessageBox::information(this,"НЕ ОК", "Произошла ошибка. Возможно, такой должости не существует");
                    qDebug() << qry.lastError();
                }
            }
            else
                { QMessageBox::information(this,"НЕ ОК", "В инициалах врача не может быть пробелов."); }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Проверьте правильность номера");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void EditDocTableForm::on_DelButton_clicked()
{
    if (!ui->DelNumEdit->text().isEmpty())
    {
        QSqlQuery qry(db);
        qry.prepare("DELETE FROM doctors WHERE Doctor_Number = :DN");
        qry.bindValue(":DN", ui->DelNumEdit->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Врача с таким номером не существует");
            else
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Врач удален");
            }
        }
        else
        {
            QMessageBox::information(this,"НЕ ОК", "Во время удаления произошла ошибка");
            qDebug() << qry.lastError();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили поле, необходимое для удаления.");
}

void EditDocTableForm::on_pushButton_clicked()
{
    if ((!ui->DocNumVac->text().isEmpty()) && (!ui->StartDate->text().isEmpty()) && (!ui->EndDate->text().isEmpty()))
    {
        QSqlQuery qry(db);
        if (ui->StartDate->date() < ui->EndDate->date())
        {
            qry.prepare("INSERT INTO vacations (Doctor_Number, Start_Date, End_Date) VALUES (:DN, :SD, :ED)");
            qry.bindValue(":DN", ui->DocNumVac->text());
            qry.bindValue(":SD", ui->StartDate->text());
            qry.bindValue(":ED", ui->EndDate->text());
            if (qry.exec())
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Отпуск добавлен");
            }
            else
            {
                if (qry.lastError().number() == 1062)
                    QMessageBox::information(this,"НЕ ОК", "У этого врача уже есть отпуск");
                else if (qry.lastError().number() == 1452)
                    QMessageBox::information(this,"НЕ ОК", "Врача с таким номером не существует");
                else
                    QMessageBox::information(this,"НЕ ОК", "Во время добавления произошла ошибка");
                qDebug() << qry.lastError();
            }
        }
        else
        {
            QMessageBox::information(this,"HE ОК", "Отпуск не добавлен. Проверьте логичность выставленных дат");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void EditDocTableForm::on_pushButton_2_clicked()
{
    if ((!ui->DocNumVac->text().isEmpty()) && (!ui->StartDate->text().isEmpty()) && (!ui->EndDate->text().isEmpty()))
    {
        QSqlQuery qry(db);
        if (ui->StartDate->date() < ui->EndDate->date())
        {
            qry.prepare("UPDATE vacations SET Start_Date= :SD, End_Date = :ED WHERE Doctor_Number = :DN");
            qry.bindValue(":DN", ui->DocNumVac->text());
            qry.bindValue(":SD", ui->StartDate->text());
            qry.bindValue(":ED", ui->EndDate->text());
            if (qry.exec())
            {
                if (qry.numRowsAffected() != 1)
                    QMessageBox::information(this, "НЕ ОК", "Врача с таким номером не существует");
                else
                {
                    this->WF->RefreshData();
                    QMessageBox::information(this,"ОК", "Отпуск отредактирован");
                }
            }
            else
            {
                 QMessageBox::information(this,"НЕ ОК", "Во время редактирования произошла ошибка");
                 qDebug() << qry.lastError();
            }
        }
        else
        {
            QMessageBox::information(this,"HE ОК", "Отпуск не отредактирован. Проверьте логичность выставленных дат");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void EditDocTableForm::on_AddTime_clicked()
{
    if ((!ui->DocNumTime->text().isEmpty()) && (!ui->TimeStart->text().isEmpty()) && (!ui->TimeEnd->text().isEmpty()))
    {
        QSqlQuery qry(db);
        if (ui->TimeStart->time() < ui->TimeEnd->time())
        {
            qry.prepare("INSERT INTO working_hours (Doctor_Number, Start_Time, End_Time) VALUES(:DNT, :TS, :TE)");
            qry.bindValue(":DNT", ui->DocNumTime->text());
            qry.bindValue(":TS", ui->TimeStart->text());
            qry.bindValue(":TE", ui->TimeEnd->text());
            if (qry.exec())
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Время успешно добавлено");
            }
            else
            {
                if (qry.lastError().number() == 1062)
                    QMessageBox::information(this,"НЕ ОК", "У этого врача уже есть время работы");
                else if (qry.lastError().number() == 1452)
                    QMessageBox::information(this,"НЕ ОК", "Врача с таким номером не существует");
                else
                    QMessageBox::information(this,"НЕ ОК", "Во время добавления произошла ошибка");
                 qDebug() << qry.lastError();
            }
        }
        else
        {
            QMessageBox::information(this,"HE ОК", "Вермя работы не добавлено. Проверьте логичность выставленного времени");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void EditDocTableForm::on_EditTime_clicked()
{
    if ((!ui->DocNumTime->text().isEmpty()) && (!ui->TimeStart->text().isEmpty()) && (!ui->TimeEnd->text().isEmpty()))
    {
        QSqlQuery qry(db);
        if (ui->TimeStart->time() < ui->TimeEnd->time())
        {
            qry.prepare("UPDATE working_hours SET Start_Time = :TS, End_Time = :TE WHERE Doctor_Number = :DNT");
            qry.bindValue(":DNT", ui->DocNumTime->text());
            qry.bindValue(":TS", ui->TimeStart->text());
            qry.bindValue(":TE", ui->TimeEnd->text());
            if (qry.exec())
            {
                if (qry.numRowsAffected() != 1)
                    QMessageBox::information(this, "НЕ ОК", "Врача с таким номером не существует");
                else
                {
                    this->WF->RefreshData();
                    QMessageBox::information(this,"ОК", "Время отредактировано");
                }
            }
            else
            {
                 QMessageBox::information(this,"НЕ ОК", "Во время редактирования произошла ошибка");
                 qDebug() << qry.lastError();
            }
        }
        else
        {
            QMessageBox::information(this,"HE ОК", "Вермя работы не отредактировано. Проверьте логичность выставленного времени");
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void EditDocTableForm::on_ReturnToTable_clicked()
{
    this->WF->RefreshData();
    EditDocTableForm::hide();
}

void EditDocTableForm::on_AddCab_clicked()
{
    if ((!ui->Doc_Number_Cab->text().isEmpty()) && (!ui->Doc_Cab->text().isEmpty()))
    {
        QSqlQuery qry(db);
        qry.prepare("INSERT INTO cabinets (Doctor_Number, Cabinet_Number) VALUES(:DNC, :DC)");
        qry.bindValue(":DNC", ui->Doc_Number_Cab->text());
        qry.bindValue(":DC", ui->Doc_Cab->text());
        if (qry.exec())
        {
            QMessageBox::information(this,"ОК", "Врач добавлен в кабинет");
            this->WF->RefreshData();
        }
        else
        {
            if (qry.lastError().number() == 1062)
                QMessageBox::information(this,"НЕ ОК", "К этому врачу уже привязан кабинет");
            else if (qry.lastError().number() == 1452)
                QMessageBox::information(this,"НЕ ОК", "Врача с таким номером не существует");
            else
                QMessageBox::information(this,"НЕ ОК", "Во время добавления произошла ошибка");
             qDebug() << qry.lastError();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для добавления");
}

void EditDocTableForm::on_EditCab_clicked()
{
    if ((!ui->Doc_Number_Cab->text().isEmpty()) && (!ui->Doc_Cab->text().isEmpty()))
    {
        QSqlQuery qry(db);
        qry.prepare("UPDATE cabinets SET Cabinet_Number= :DC WHERE Doctor_Number = :DNC");
        qry.bindValue(":DNC", ui->Doc_Number_Cab->text());
        qry.bindValue(":DC", ui->Doc_Cab->text());
        if (qry.exec())
        {
            if (qry.numRowsAffected() != 1)
                QMessageBox::information(this, "НЕ ОК", "Врача с таким номером не существует");
            else
            {
                this->WF->RefreshData();
                QMessageBox::information(this,"ОК", "Кабинет отредактирован");
            }
        }
        else
        {
             QMessageBox::information(this,"НЕ ОК", "Во время редактирования произошла ошибка");
             qDebug() << qry.lastError();
        }
    } else QMessageBox::information(this,"НЕ ОК", "Вы не заполнили одно или несколько полей необходимых для редактирования");
}

void EditDocTableForm::on_Doc_Number_editingFinished()
{

}
