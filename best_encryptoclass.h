#ifndef BEST_ENCRYPTOCLASS_H
#define BEST_ENCRYPTOCLASS_H
#include <QString>
#include <QCryptographicHash>
#include <QDebug>
#include <QMessageAuthenticationCode>
#include "bestreadxml.h"

class Best_EncryptoClass
{
public slots:
    QString encryptoString(const QString & str);
};

#endif // BEST_ENCRYPTOCLASS_H
